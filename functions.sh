#!/bin/bash
function fn_prompt {
	read -p "$1 " TEMP
	echo "$TEMP"
}

function fn_yn {
	local yn
	while true; do
		yn=`fn_prompt "${1}"`
		case $yn in
			[Yy]* ) echo 1; break;;
			[Nn]* ) echo 0; break;;
			* ) echo "Please answer with YES or NO";;
		esac
	done
}

function fn_output {
	echo -e "\n\n$1"
}

function fn_deploy_symlink {
	mkdir -p `dirname "$2"`
	# local yes=`fn_yn "Deploy $2?"`
	local yes=1
	if [[ $yes -eq 1 ]]; then
		if [[ $MODE -eq 0 ]]; then
			cat "${DIR}/misc/spacer" >> "$2"
			cat "$1" >> "$2"
		else
			[[ -e "$2" ]] && mv "$2" "$2.orig"
			ln -f -s "$1" "$2"
		fi
	fi
}

function fn_deploy_copy {
	mkdir -p `dirname "$2"`
	# local yes=`fn_yn "Deploy $2?"`
	local yes=1
	if [[ $yes -eq 1 ]]; then
		if [[ $MODE -eq 0 ]]; then
			cat "${DIR}/misc/spacer" >> "$2"
			cat "$1" >> "$2"
		else
			[[ -f "$2" ]] && mv "$2" "$2.orig"
			cp "$1" "$2"
		fi
	fi
}

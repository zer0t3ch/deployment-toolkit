#!/bin/bash
PS3="--> "
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
MODE=0

source ${DIR}/functions.sh

fn_output "Would you prefer to append scripts to what already exists or backup and replace?"
select APPEND in Append Replace; do
	case $APPEND in
		Append	)
			MODE=0
			break;;
		Replace	)
			MODE=1
			fn_output ".orig will be appended to filenames"
			break;;
	esac
done

fn_deploy_symlink "${DIR}/bash/rc" "${HOME}/.bashrc"
fn_deploy_symlink "${DIR}/bash/aliases" "${HOME}/.bash_aliases"
fn_deploy_symlink "${DIR}/bash/profile" "${HOME}/.bash_profile"
fn_deploy_symlink "${DIR}/bash/colors" "${HOME}/.bash_colors"
fn_deploy_symlink "${DIR}/misc/tmux.conf" "${HOME}/.tmux.conf"
fn_deploy_symlink "${DIR}/misc/xinitrc" "${HOME}/.xinitrc"
fn_deploy_symlink "${DIR}/i3/config" "${HOME}/.config/i3/config"
fn_deploy_symlink "${DIR}/misc/vimrc" "${HOME}/.vimrc"

elevate=$(fn_yn "Would you like to elevate via sudo and deploy to /etc/skel?")
[ ${elevate} -eq 1 ] && sudo -s ${DIR}/elevated-setup.sh

#!/bin/bash
PS3="--> "
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
MODE=0

source ${DIR}/functions.sh

fn_output "Would you prefer to append scripts to what already exists or backup and replace?"
select APPEND in Append Replace; do
	case $APPEND in
		Append	)
			MODE=0
			break;;
		Replace	)
			MODE=1
			fn_output ".orig will be appended to filenames"
			break;;
	esac
done

fn_deploy_copy "${DIR}/bash/rc" "/etc/skel/.bashrc"
fn_deploy_copy "${DIR}/bash/aliases" "/etc/skel/.bash_aliases"
fn_deploy_copy "${DIR}/bash/profile" "/etc/skel/.bash_profile"
fn_deploy_copy "${DIR}/bash/colors" "/etc/skel/.bash_colors"
fn_deploy_copy "${DIR}/misc/tmux.conf" "/etc/skel/.tmux.conf"
fn_deploy_copy "${DIR}/misc/xinitrc" "/etc/skel/.xinitrc"
fn_deploy_copy "${DIR}/i3/config" "/etc/skel/.config/i3/config"
fn_deploy_copy "${DIR}/misc/vimrc" "/etc/skel/.vimrc"

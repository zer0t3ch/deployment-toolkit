#!/bin/bash

# STEAMAPPS=~/.steamhome/.steam/steam/steamapps
STEAMAPPS=/mnt/ssdata/steamlin/steamapps

echo '<openbox_pipe_menu>'
echo '<item label="Steam"><action name="Execute"><execute>steam</execute></action></item>'
echo '<separator/>'
for file in $(ls $STEAMAPPS/*.acf -1v); do
    ID=$(cat "$file" | grep '"appID"' | head -1 | sed -r 's/[^"]*"appID"[^"]*"([^"]*)"/\1/')
    NAME=$(cat "$file" | grep '"name"' | head -1 | sed -r 's/[^"]*"name"[^"]*"([^"]*)"/\1/')
    echo "<item label=\"$NAME\"><action name=\"Execute\"><execute>steam steam://run/$ID</execute></action></item>"
done
echo '</openbox_pipe_menu>'

# echo '<openbox_pipe_menu>'
# echo '<item label="Steam"><action name="Execute"><execute>steam</execute></action></item>'
# echo '<separator/>'
# cat $(ls $STEAMAPPS/*.acf -1v) | egrep '"name"|"GameID"' | sed -r 's/"name"[\t ]+"([^"]+)"/<item label="\1">/' | sed -r 's/"GameID"[\t ]+"([^"]+)"/<action name="Execute"><execute>steam steam steam:\/\/run\/\1<\/execute><\/action><\/item>/' | sed 'N;s/\n//' |sed 's/\t//g' | sort
# echo '</openbox_pipe_menu>'